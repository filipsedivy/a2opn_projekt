namespace Projekt.Source
{
    public interface ISource
    {
        string GetSource();
    }
}