namespace Projekt.Source
{
    public class Lupa : AbstractSource
    {
        public override string GetSource()
        {
            return "https://www.lupa.cz/rss/clanky-samostatne/";
        }
    }
}