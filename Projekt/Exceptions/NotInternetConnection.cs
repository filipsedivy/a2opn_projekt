using System;

namespace Projekt.Exceptions
{
    public class NotInternetConnection : Exception
    {
        public NotInternetConnection()
        {
        }

        public NotInternetConnection(string message) : base(message)
        {
        }

        public NotInternetConnection(string message, Exception inner) : base(message, inner)
        {
        }
    }
}