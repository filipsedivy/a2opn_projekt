using System;
using System.Threading;

namespace Projekt.Logger
{
    public class ConsoleLogger : ILogger
    {
        public static ILogger GetInstance()
        {
            return new ConsoleLogger();
        }

        public void Success(string text)
        {
            ColorPrefixWrite(ConsoleColor.Green, "Správně");

            WriteLine(text);
        }

        public void Info(string text)
        {
            ColorPrefixWrite(ConsoleColor.DarkYellow, "Informace");

            WriteLine(text);
        }

        public void Error(string text)
        {
            ColorPrefixWrite(ConsoleColor.Red, "Chyba");

            WriteLine(text);
        }

        public void Write(string text)
        {
            Console.Write(text);
        }

        public void WriteLine(string text)
        {
            Console.WriteLine(text);
        }

        public void Clear(int timeout = 0)
        {
            if (timeout > 0)
            {
                for (var i = 0; i < timeout; i++)
                {
                    Info($"Pokračování za {timeout - i} sekund");
                    Thread.Sleep(1000);
                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    ClearCurrentConsoleLine();
                }
            }

            Console.Clear();
        }

        public void UpCursorPosition(int lines)
        {
            SetCursorPosition(Console.CursorTop - lines);
        }

        public void SetCursorPosition(int top)
        {
            Console.SetCursorPosition(0, top);
        }

        public void ClearCurrentConsoleLine()
        {
            var currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }

        private void ColorPrefixWrite(ConsoleColor consoleColor, string text)
        {
            var lastColor = Console.ForegroundColor;

            Write("[");
            Console.ForegroundColor = consoleColor;

            Write(text);
            Console.ForegroundColor = lastColor;

            Write("] ");
        }
    }
}