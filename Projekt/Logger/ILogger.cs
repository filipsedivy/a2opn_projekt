namespace Projekt.Logger
{
    public interface ILogger
    {
        void WriteLine(string text);

        void Write(string text);

        void Success(string text);

        void Info(string text);

        void Error(string text);

        void Clear(int timeout = 0);
    }
}