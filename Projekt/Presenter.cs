using System;
using System.Globalization;
using System.Net;
using System.Threading;
using System.Xml.Linq;
using Projekt.Client;
using Projekt.External;
using Projekt.Logger;
using Projekt.Model;
using Projekt.Source;

namespace Projekt
{
    public class Presenter
    {
        private ISource _source;

        private ILogger _logger;

        private RssItems _items;

        public Presenter(ISource source, ILogger logger)
        {
            _source = source;
            _logger = logger;
            _items = new RssItems();
        }

        public void Render()
        {
            using var wc = new WebClient();

            _logger.Info("Stahuji vzdálený obsah");
            var contents = wc.DownloadString(_source.GetSource());

            Thread.Sleep(800);
            if (_logger is ConsoleLogger consoleLogger)
            {
                consoleLogger.UpCursorPosition(1);
                consoleLogger.ClearCurrentConsoleLine();
            }

            var document = XDocument.Parse(contents);

            foreach (var item in document.Descendants("item"))
            {
                var title = item.Element("title").Value;
                var description = item.Element("description").Value;
                var pubDateValue = item.Element("pubDate").Value;

                var culture = CultureInfo.CurrentCulture;
                const DateTimeStyles styles = DateTimeStyles.AssumeUniversal;

                if (DateTime.TryParse(pubDateValue, culture, styles, out var pubDate))
                {
                    pubDate = DateTime.SpecifyKind(pubDate, DateTimeKind.Utc);
                }

                _items.Add(title, description, pubDate);
            }

            var rssItemsClient = new RssItemsClient();
            _items.ProcessItems(rssItemsClient.AddItem);

            _logger.Info($"Celkový počet článků: {rssItemsClient.countItems}");

            // Výpis RSS článků

            var table = new ConsoleTable("Zveřejnění", "Titulek");
            table.Options.EnableCount = false;

            foreach (var item in _items.GetList())
            {
                if(item is CzechRssItem czechRssItem)
                    table.AddRow(czechRssItem.FormatPubDate(), czechRssItem.Title);

                else
                    table.AddRow(item.FormatPubDate(), item.Title);
            }

            table.Write();
        }
    }
}