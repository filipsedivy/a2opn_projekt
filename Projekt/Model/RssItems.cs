using System;
using System.Collections.Generic;

namespace Projekt.Model
{
    public delegate void ProcessRssItemsDelegate(RssItem rssItem);

    public class RssItems
    {
        private readonly List<RssItem> _list;

        public RssItems()
        {
            _list = new List<RssItem>();
        }

        public void Add(RssItem item)
        {
            _list.Add(item);
        }

        public void Add(string title, string description, DateTime pubDate)
        {
            var obj = new RssItem(title, description, pubDate);
            Add(obj);
        }

        public List<RssItem> GetList()
        {
            return _list;
        }

        public void ProcessItems(ProcessRssItemsDelegate processRssItemsDelegate)
        {
            foreach (var item in _list)
            {
                processRssItemsDelegate(item);
            }
        }
    }
}