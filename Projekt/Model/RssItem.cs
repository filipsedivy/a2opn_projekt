using System;

namespace Projekt.Model
{
    public class RssItem
    {
        public readonly string Title;

        public readonly string Description;

        public readonly DateTime PubDate;

        public RssItem(string title, string description, DateTime pubDate)
        {
            Title = title;
            Description = description;
            PubDate = pubDate;
        }

        public virtual string FormatPubDate()
        {
            return PubDate.ToString("MM/dd/yyyy H:mm");
        }
    }
}