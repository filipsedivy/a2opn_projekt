using System;

namespace Projekt.Model
{
    public class CzechRssItem : RssItem
    {
        public CzechRssItem(string title, string description, DateTime pubDate)
            : base(title, description, pubDate)
        {
        }

        public override string FormatPubDate()
        {
            return PubDate.ToString("dd. MM. yyyy HH:mm");
        }
    }
}