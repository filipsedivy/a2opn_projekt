using Projekt.Logger;
using Projekt.Source;

namespace Projekt
{
    public class Application
    {
        private readonly bool _checkInternetConnection;

        private readonly ILogger _logger;

        public Application(ILogger logger, bool checkInternetConnection = true)
        {
            _checkInternetConnection = checkInternetConnection;
            _logger = logger;
        }

        public void Run()
        {
            if (_checkInternetConnection)
            {
                Utils.Internet.ProcessCheckConnection(_logger);
                _logger.Clear(3);
            }

            var presenter = new Presenter(new Lupa(), _logger);
            presenter.Render();
        }
    }
}