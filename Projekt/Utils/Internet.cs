using System;
using System.Net;
using System.Threading;
using Projekt.Exceptions;
using Projekt.Logger;

namespace Projekt.Utils
{
    public class Internet
    {
        public static bool CheckConnection()
        {
            try
            {
                using var client = new WebClient();
                using (client.OpenRead("http://google.com/generate_204"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static void ProcessCheckConnection(ILogger logger, int totalCheckConnections = 3)
        {
            logger.Info("Probíhá kontrola připojení k internetu");


            for (var i = 0; i < totalCheckConnections; i++)
            {
                logger.Info($"Probíhá {i + 1} ze {totalCheckConnections} kontrol");

                if (CheckConnection())
                {
                    logger.Success($"{i + 1} kontrola probělha v pořádku");
                }
                else
                {
                    throw new NotInternetConnection();
                }
                
                Thread.Sleep(300);
            }
            
            logger.Success("Připojení k internetu je k dispozici");
        }
    }
}