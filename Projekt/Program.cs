﻿using System;
using Projekt.Exceptions;
using Projekt.Logger;

namespace Projekt
{
    class Program
    {
        static void Main(string[] args)
        {
            var logger = ConsoleLogger.GetInstance();
            var application = new Application(logger);

            try
            {
                application.Run();
            }
            catch (NotInternetConnection)
            {
                logger.Error("Připojení k internetu není k dispozici");
            }
        }
    }
}